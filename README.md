# DNI 3.0 #
sus  características  fiscas,  eléctricas  y  de  comunicaciones se  rigen  por los estándaresISO7816y  el  ISO14443.Cabe  señalarqueel  DNIe  es  compatible  con  ambas implementacionesde ISO 14443.  
NFC está  basadao en  ISO  14443  y FeliCa y sigue los estándares definidos por el NFC Forum  

## ISO 14443 ## 
Es el estándar internacional para tarjetas inteligentes sin contacto para tarjetas que operan a una distancia de menos de 10 cm y a una frecuencia de 13.56 MHz. Este estándar define una tarjeta de proximidad utilizada para identificación y pagos. Se basa en un sistema RFID (Radio Frequency Identification) que utiliza un lector con unaantena  que  opera  a  la  frecuencia  indicada  anteriormente.  El  lector  mantiene  a  su alrededor un campo electromagnético del que se alimentará eléctricamente una tarjeta que se aproxime aél.Acontinuación, se podrá establecer una comunicación entre el lector y la tarjeta.  
Presenta dos tipos diferentes, tipo A y tipo B, cuya principal diferencia son el método  de  modulación  y  de  codificación,  las  velocidades  de  transmisión  y  en  los protocolos de inicialización. Dicho estándar se divide en cuatro partes:  

    • Parte 1: especifica las características físicas de la tarjeta. El tamaño de la tarjeta es el mismoque está especificadoen ISO 7816.
    • Parte 2:  detalla  la potencia  de  radiofrecuencia  y  la  interfaz  de  potencia.  Esta sección  describe  las  características técnicas  del  chip  sin  contacto, incluyendo parámetros  de  frecuencia,  velocidad  de  datos,  modulación  y  procedimientos  de codificación de bits. 
    • Parte 3: determina las funciones de inicialización y anticolisión entre tarjetas. La inicialización describe los requisitos para que el lector y la tarjeta establezcan una comunicación cuando la tarjeta entra en el campo de RF del lector. La anticolisión define  lo  que  ocurre  cuando  varias  tarjetas  entran  en  un  campo  magnético  al mismo tiempo, describiendo como el sistema determina con que tarjeta comunicarse. 
    • Parte 4: especifica el protocolo de transmisión. Esta sección define el formato de datos  y  los  elementos  de  datos  que  permiten  la  comunicación  durante  una transmisión de información  

## Arquitectura ##
El cerebro del DNI electrónico es el chip ST19WL34 y utiliza el sistema operativoDNIe v4.0, de propósito específico. Tiene las siguientes características:  

    • Una memoria Flash de 400 kB de memoria Flash (código y personalización).
    • Una memoria RAM de 8 kB.•UnchipDual Interface, lo que permite una conexión con o sin contactos.•Una criptolibreria RSA.

La  información  en  el  chip,  organizada  en  ficheros,está  distribuida  en treszonas  con diferentes niveles y condiciones de acceso:  

    • Zona pública: Accesible en lectura sin restricciones y contiene:  
        o Un certificado CA intermedia emisora.
        o Las claves de Diffie-Hellman.
        o El certificado x509 de componente.
    • Zona privada: Accesible en lectura al ciudadano utilizando el PIN, conteniendo:
        o El certificado de Firma (no repudio).
        o El certificado de Autenticación (Digital Signature).
    • Zona  de  seguridad:  Accesible  en  lectura  por  el  ciudadano,  en  los  Puntos  de Actualización del DNI(PAD). Contenido:
        o Los datos de filiación del usuario y contenidos en el soporte físico del DNI.
        o La imagen de la fotografía.oLa imagen de la firma manuscrita.

Enlace interesante:  
https://www.ccn-cert.cni.es/documentos-publicos/x-jornadas-stic-ccn-cert/1939-p2-04-dni30-universidad-zaragoza/file.html

## Datos que pueden extraerse mediante NFC del DNI 3.0 ##
### Data Group 1 ###
    • Nombrey apellidos: cadena
	• Nacionalidad: cadena
	• Sexo: cadena
	• Nº de soporte: cadena
	• Fechade expiración: cadena
	• Fechade nacimiento: cadena
	• Emisor: cadena(valor “España”)
	• OptData: null
	• Tipode documento: cadena(valor "ID")
### Data Group 2 ###
	• Imagen facial (formatoJPEG2000, sin metadatos)
### Data Group 7 ###
	• Imagen de la firma (formatoJPEG2000, sin metadatos)
### Data Group 11 ###
	• Dirección: cadena
	• Nº DNI: cadena
	• Lugardenacimiento: cadena(e.g., valor“ZARAGOZA<ZARAGOZA”)
	• Title: null
	• Teléfono: null
	• Profesión: null
	• CustodyInfo: null
	• ICAOName: null
	• OtherInfo: null
	• Summary: null

El DNIe contiene dos tipos de datos biométricos: la imagen y las huellas dactilares de los dedos índices del titular. Según ICAO 9303, la imagen corresponde con el datagroup 2 y el datagroup  3 contiene  la  huella  dactilar.  Sin  embargo,  no  es  posible  acceder  a  la información del datagroup 3,siendo únicamente accesible para la DGPcuando necesite dicha información

Se pueden acceder a estos datos de dos formas:  

    • Con contactos: para poder leerla informaciónes necesarioun lector de tarjetas inteligentes que cumpla el estándar ISO 7816y tener instaladosen el ordenador los controladores del lector y los módulos criptográficos del DNIe.Entonces, se introduce el DNIe al lector de tarjetas y yase podráutilizar cuando se requiera el uso de los certificados.
    • Sin contactos: para acceder a los datos es necesario acercar el DNIe aunlector de tarjetas sin contactos compatible con ISO 14443 o aunteléfono inteligente con el NFC activado. Como medida de seguridad el DNIe implementa la necesidad de garantizar  la  identidad  del  dispositivo  que  se  comunica  con  él  y  el  control  por parte  del  usuario de  este.  La aplicación  en  cuestión  pedirá  introducir  elCAN (Figura  4-2),  para  formalizar  es  vínculo  y  permitir  ese  acceso.  El  CAN  es  una clave  numérica  de  seis  números  que  se  encuentra  en  el  anverso  del  DNIe  3.0  y permite establecer una conexión cifrada para el intercambio de datos entre el DNIe y el lector.Una vez verificado el CAN y establecida la conexión, el dispositivo ya es capaz de leer los datosy usar los certificados usando los mismos controladores y módulos criptográficos que en el caso anterior.  

https://repositorio.unican.es/xmlui/bitstream/handle/10902/19538/428327.pdf?sequence=1&isAllowed=y

## Protocolos de comunicación NFC ##

### Protocolo BAC (Basic Access Control) ###
Usa Estándar ICAO (Machine Readable Travel Documents -Part 11: Security Mechanisms for MRTDs, 2015, http://www.icao.int/publications/Documents/9303_p11_cons_en.pdf)
https://techblog.bozho.net/electronic-machine-readable-travel-documents/
#### Datos necesarios para empezar la comunicación ####
	• Número de soporte (3 + 6)
	• Fecha de nacimiento (aammdd)
	• Fecha de expiración (aammdd)

### Procolo PACE (Password Authenticated Connection Establishment) ###
Protocolo Diffie-Hellman para generar claves de sesión

#### Datos necesarios para empezar la comunicación ####
	• CAN (Card Access Number): número que se encuentra en la esquina inferior izquierda, en la parte frontal

PACE proporciona un mecanismo para generar claves de sesión fuertes independientemente de  la  fortaleza  de  la  contraseña.  La  contraseña  o  información  inicial  puede  tener  una entropía baja (por ej. 6 caracteres son suficientes). Los  datos  de  la  contraseña se  pueden  obtener,  bien  a  partir  de  la  información  MRZ, accesible visualmente desde el documento; o bien a partir de un número CAN (Card Access Number)  conocido  por  el  portador  del  documento  (información  secreta)  o  que  puede  estar impreso en el documento y ser accesible visualmente. 

#### 
El chip también incluye la imagen, pero hay que averiguar si es posible acceder al usar el lector de DNI.
Según indica la web la imagen está en: ** Zona de seguridad: Accesible en lectura por el ciudadano, en los Puntos de Actualización del DNI 3**
https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1078&id_menu=[26_%2030]

https://github.com/E3V3A/JMRTD/tree/master/jmrtd/src/org/jmrtd

## Otro enlaces
https://webdiis.unizar.es/~ricardo/files/TFGs/SeguridadDNIe3.0_NFC.pdf#cite.http%3A//www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action%3Fpag%3DREF_1120  
**Demo de aplicaciones desarrolladas por la Policía Nacional**  
https://www.dnielectronico.es/PortalDNIe/PRF1_Cons02.action?pag=REF_1120  
https://www.dnielectronico.es/PDFs/Manual_de_Comandos_para_Desarrolladores_102.pdf  
https://es.stackoverflow.com/questions/240785/dnie-3-0-protocolo-pace-bouncycastle  
https://stackoverflow.com/questions/21849601/android-nfc-read-data-from-epassport  
[PFC - VERIFICADOR MÓVIL DEDOCUMENTOS DE TRÁNSITO](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwj4wo_a0fXsAhUphOAKHY0HDcQQFjAAegQIBBAC&url=https%3A%2F%2Fe-archivo.uc3m.es%2Fbitstream%2Fhandle%2F10016%2F24179%2FPFC_Javier_Hernandez_Fisac_2015.pdf&usg=AOvVaw1iqfwm6eIyRD2MjeZXotbQ)  
https://repositorio.unican.es/xmlui/bitstream/handle/10902/19538/428327.pdf?sequence=1&isAllowed=y  

# Pasaportes #
https://github.com/Glamdring/epassport-reader

**Aplicación desarrollada para Android que es capaz de obtener los datos necesarios**  
https://github.com/tananaev/passport-reader



## Librería Open Source 
http://jmrtd.org/
### Documentación
https://www.javadoc.io/doc/org.jmrtd/jmrtd/latest/index.html

